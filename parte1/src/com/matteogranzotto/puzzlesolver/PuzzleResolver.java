package com.matteogranzotto.puzzlesolver;

import java.util.ArrayList;
import java.util.Date;

public class PuzzleResolver
{

    /*Variabili*/
    private ArrayList<PieceOfPuzzle> array;
    private Puzzle puzzle;

    /*Costruttori*/
    //Costruttore di default.
    public PuzzleResolver()
    {
        array=null;
        puzzle=null;
    }

    //Costruttore ad un parametro
    public PuzzleResolver(ArrayList<PieceOfPuzzle> array) throws ExceptionPS
    {
        this.array=array;
        Solver();
    }

    /*Metodi */
    //Ritorna un Puzzle corretto
    public Puzzle getPuzzleSolved ()
   {
        return puzzle;
   }

    //Ritorna un Puzzle corretto, come parametro riceve un array di PieceOfPuzzle che dovra' risolvere.
    public Puzzle getPuzzleSolved (ArrayList<PieceOfPuzzle> array) throws ExceptionPS
    {
        this.array=array;
        Solver();
        return puzzle;
    }

    //Posso sapere se l'array di PieceOfPuzzle � stato risolto e posso ottenerlo con la chiamata getPuzzleSolved
    public boolean isResolve()
    {
        return array==null;
    }

    //Posso sapere se PuzzleResolver ha ricevuto in input un array di PieceOfPuzzle
    public boolean isInizializate()
    {
        return puzzle==null && array==null;
    }

    /*Metodi privati*/
    private void Solver() throws ExceptionPS
    {
        int c=trovaNumeroColonne();
        int r=trovaNumeroRighe();

        puzzle=new Puzzle(r,c);
        String nord="VUOTO";
        String ovest="VUOTO";
        int indice=0;

        PieceOfPuzzle primoelemento =trovaPrimoElementoDiagonale(nord,ovest);
        try
        {
            while (primoelemento != null)
            {
                puzzle.insertElement(primoelemento, indice, indice);
                setColonnaDopoElemento(getColonnaAPartireDaElemento(primoelemento), indice);
                setRigaDopoElemento(getRigaAPartireDaElemento(primoelemento), indice);
                if (indice + 1 < c)
                {
                    nord = primoelemento.getEst();
                }
                if (indice + 1 < r)
                {
                    ovest = primoelemento.getSud();
                }
                indice++;
                primoelemento = trovaPrimoElementoDiagonale(nord, ovest);
            }
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            throw new ExceptionPS("Superato limite massimo dell'array." );
        }
    } // dopo il metodo Solver esiste un oggetto di tipo Puzzle risolto.

    //Trova il primo elemento della diagonale
    private PieceOfPuzzle trovaPrimoElementoDiagonale(String nord, String ovest) throws ExceptionPS //trova il primo elemento della diagonale e lo toglie dall'array
    {
        PieceOfPuzzle temp=null;

        try
        {
            for (int i = 0; i < array.size(); i++)
            {
                if (array.get(i).getNord().equalsIgnoreCase(nord) && array.get(i).getOvest().equalsIgnoreCase(ovest))
                {
                    temp = array.get(i);
                    array.remove(i);
                }
            }
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            throw new ExceptionPS("Superato limite massimo dell'array." );
        }
        return temp;
    }

    //Trova il numero di colonne che comporranno il Puzzle
    private int trovaNumeroColonne() throws ExceptionPS
    {
        int cont=0;

        try
        {
            for (int i = 0; i < array.size(); i++)
            {
                if (array.get(i).getNord().equalsIgnoreCase("VUOTO" ))
                {
                    cont++;
                }
            }
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            throw new ExceptionPS("Superato limite massimo dell'array.");
        }
        return cont;
    }


    //Trova il numero di righe che comporranno il Puzzle
    private int trovaNumeroRighe() throws ExceptionPS
    {
        int cont=0;

        try
        {
            for (int i = 0; i < array.size(); i++)
            {
                if (array.get(i).getOvest().equalsIgnoreCase("VUOTO" ))
                {
                    cont++;
                }
            }
        }
        catch(ArrayIndexOutOfBoundsException e)
        {
            throw new ExceptionPS("Superato limite massimo dell'array.");
        }
        return cont;
    }

    //Compone una riga a partire da un PieceOfPuzzle
    private ArrayList<PieceOfPuzzle> getRigaAPartireDaElemento(PieceOfPuzzle elemento) throws ExceptionPS
    {
        ArrayList<PieceOfPuzzle> temp = new ArrayList<PieceOfPuzzle>();
        PieceOfPuzzle ultimo = elemento;

        try
        {
            while (!ultimo.getEst().equalsIgnoreCase("VUOTO" ))
            {
                boolean stop = false;
                try
                {
                    for (int i = 0; i < array.size() && !stop; i++)
                    {
                        if (array.get(i).getOvest().equalsIgnoreCase(ultimo.getIdPezzo()))
                        {
                            temp.add(array.get(i));
                            ultimo = array.get(i);
                            array.remove(i);
                            stop = true;
                        }
                    }
                }
                catch (ArrayIndexOutOfBoundsException e)
                {
                    throw new ExceptionPS("Superato limite massimo dell'array." );
                }
            }
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            throw new ExceptionPS("Superato limite massimo dell'array." );
        }
        return temp;
    }

    //Compone una colonna a partire da un PieceOfPuzzle
    private ArrayList<PieceOfPuzzle> getColonnaAPartireDaElemento(PieceOfPuzzle elemento) throws ExceptionPS
    {
        ArrayList<PieceOfPuzzle> temp = new ArrayList<PieceOfPuzzle>();
        PieceOfPuzzle ultimo = elemento;

        try
        {
            while (!ultimo.getSud().equalsIgnoreCase("VUOTO" ))
            {
                boolean stop = false;
                try
                {
                    for (int i = 0; i < array.size() && !stop; i++)
                    {

                        if (array.get(i).getNord().equalsIgnoreCase(ultimo.getIdPezzo()))
                        {
                            temp.add(array.get(i));
                            ultimo = array.get(i);
                            array.remove(i);
                            stop = true;
                        }
                    }
                }
                catch (ArrayIndexOutOfBoundsException e)
                {
                    throw new ExceptionPS("Superato limite massimo dell'array." );
                }
            }
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            throw new ExceptionPS("Superato limite massimo dell'array." );
        }
        return temp;
    }


    //Inserisce in riga un numero di elementi a partire da un elemento dato
    private void setRigaDopoElemento(ArrayList<PieceOfPuzzle> arrayp,int elementoDellaDiagonale) throws ExceptionPS
    {
        try
        {
            for (int i = elementoDellaDiagonale; i < arrayp.size() + elementoDellaDiagonale; i++)
            {
                puzzle.insertElement(arrayp.get(i - elementoDellaDiagonale), elementoDellaDiagonale, i + 1);
            }
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            throw new ExceptionPS("Superato limite massimo dell'array." );
        }
    }

    //Inserisce in colonna un numero di elementi a partire da un elemento dato
    private void setColonnaDopoElemento(ArrayList<PieceOfPuzzle> arrayp,int elementoDellaDiagonale) throws ExceptionPS
    {
        try
        {
            for (int i = elementoDellaDiagonale; i < arrayp.size() + elementoDellaDiagonale; i++)
            {
                puzzle.insertElement(arrayp.get(i - elementoDellaDiagonale), i + 1, elementoDellaDiagonale);
            }
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            throw new ExceptionPS("Superato limite massimo dell'array." );
        }
    }

} //Fine classe

/**
 * L'oggetto puo essere usato in due modi:
 * I)  Creo un oggetto di tipo PuzzleResolver passandogli come unico parametro un array di PieceOfPuzzle.
 *     Questo, se l'array e corretto e non vuoto, crea Puzzle risolto che si puo ottenere con la chiamata
 *     del metodo getPuzzleSolved.
 * II) Creo un oggetto vuoto di tipo PuzzleResolver. In un secondo momento chiamamo il metodo getPuzzleSolved,
 *     a cui passo un array corretto di PieceOfPuzzle, il quale restituisce un Puzzle corretto.
 */