package com.matteogranzotto.puzzlesolver;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.spec.ECField;
import java.util.ArrayList;

public class ReadPiecesFromInputFile
{

    /*Variabili private*/
    private Path inputPath;
    private ArrayList<PieceOfPuzzle> arrayOfPieces;

    /*Variabili statiche*/
    private static Charset charset = StandardCharsets.UTF_8;

    /*Costruttori*/
    public ReadPiecesFromInputFile() /*Costruttore a zero parametri*/
    {
        inputPath=null;
        arrayOfPieces=null;
    }

    public ReadPiecesFromInputFile(String input) throws ExceptionPS /*Costruttore ad un parametro, Path del file da leggere*/
    {
        Path inputPath = Paths.get(input);
        ArrayList<String> inputContent = readContent(inputPath);
        arrayOfPieces = arrayOfPieces(inputContent);
    }

    /*Metodi*/
    //Ritorna l'array di PieceOfPuzzle
    public ArrayList<PieceOfPuzzle> getArrayOfPieces()
    {
        return arrayOfPieces;
    }

    //Sovraccarico del metodo getArrayOfPieces: passo il path di un nuovo file e ritorno l'array di PieceOfPuzzle
    public ArrayList<PieceOfPuzzle> getArrayOfPuzzles(String input)
    {
        Path inputPath = Paths.get(input);
        return arrayOfPieces;
    }

    /*Metodi statici*/
    //Prende delle stringhe (che devono essere compatibili) e le trasforma in PieceOfPuzzle. Fatto cio le inserisce
    //in array di PieceOfPuzzle che restituisce.
    private static ArrayList<PieceOfPuzzle> arrayOfPieces(ArrayList<String> inputContent) throws ExceptionPS
    {
        ArrayList<PieceOfPuzzle> array=new ArrayList<PieceOfPuzzle>();
        int i = 0;

        try
        {
            for (i = 0; i < inputContent.size(); i++)
            {
                PieceOfPuzzle tessera = splitLineIntoAPieceOfPuzzle(inputContent.get(i));
                array.add(tessera);
            }
        }
        catch(ExceptionPS e)
        {
            throw new ExceptionPS("Riga sbagliata:"+i+" "+e.getError());
        }
        return array;
    }

    //Prende una stringa (che deve essere compatibile) e la trasforma in un PieceOfPuzzle che ritorna.
    private static PieceOfPuzzle splitLineIntoAPieceOfPuzzle(String line) throws ExceptionPS
    {
        PieceOfPuzzle tessera=null;

        if (line.contains("\t"))
        {
            String[] parts = line.split("\t");
            if(parts.length==6)
            {
                tessera=new PieceOfPuzzle(parts[0],parts[1],parts[2],parts[3],parts[4],parts[5]);
            }
            else
            {
                throw new ExceptionPS("Non ci sono esattamente cinque elementi.");
            }
        }
        else
        {
            throw new ExceptionPS("Non è sintatticamente corretta.");
        }
        return tessera;
    }

    //Legge riga per riga il file passatogli. Ritorna un array di stringhe (ogni elemento e' una riga).
    private static ArrayList<String> readContent(Path inputPath)  throws ExceptionPS
    {
        ArrayList<String> content = new ArrayList<String>();

        try (BufferedReader reader = Files.newBufferedReader(inputPath,
                charset))
        {
            String line = null;
            while ((line = reader.readLine()) != null)
            {
                content.add(line);
            }
        }
        catch (IOException e)
        {
            throw new ExceptionPS("Ci sono stati dei problemi nella lettura del file.");
        }
        return content;
    }

} //Fine classe

/**
 * L'oggetto puo essere usato in due modi:
 * I)  Creo un oggetto di tipo ReadPiecesFromInputFile passandogli come unico parametro il path del file da leggere.
 *     Questo, se il path contiene un file idoneo, crea l'array di PieceOfPuzzle che si puo ottenere con la chiamata
 *     del metodo getArrayOfPieces.
 * II) Creo un oggetto vuoto di tipo ReadPiecesFromInputFile. In un secondo momento chiamamo il metodo getArrayOfPieces,
 *     a cui passo il path, il quale restituisce l'array di PieceOfPuzzle.
 */