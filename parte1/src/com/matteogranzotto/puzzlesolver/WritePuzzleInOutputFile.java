package com.matteogranzotto.puzzlesolver;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class WritePuzzleInOutputFile
{

    /*Variabili*/
    private static Charset charset = StandardCharsets.UTF_8;
    private Path outputPath = null;
    private Puzzle puzzle=null;

    /*Costruttori*/
    public WritePuzzleInOutputFile(String output, Puzzle puzzle) throws ExceptionPS
    {
        writeContent(Paths.get(output),puzzle);
    }

    /*Metodi statici*/
    private static void writeContent(Path file, Puzzle puzzle) throws ExceptionPS
    {
        try (BufferedWriter writer = Files.newBufferedWriter(file, charset))
        {
            String s="";
            try
            {
                for (int i = 0; i < puzzle.numRighe(); i++)
                {
                    for (int j = 0; j < puzzle.numColonne(); j++)
                    {
                        String temp = puzzle.getElement(i, j).getCarattere();
                        writer.write(temp);
                        s = s + puzzle.getElement(i, j).getCarattere();
                    }
                    s = s + "\n";
                }
            }
            catch(ArrayIndexOutOfBoundsException e)
            {
                throw new ExceptionPS("Superato limite massimo dell'array." );
            }
            writer.newLine();
            writer.newLine();
            String[] words = s.split("\n");
            for (String word: words)
            {
                writer.write(word);
                writer.newLine();
            }
            writer.newLine();
            writer.write(puzzle.numRighe()+" "+puzzle.numColonne());
        }
        catch(ArrayIndexOutOfBoundsException e)
        {
            throw new ExceptionPS("Superato limite massimo dell'array." );
        }
        catch (IOException e)
        {
            throw new ExceptionPS("Ci sono stati dei problemi nella scrittura del file.");
        }
    }

} // Fine classe
