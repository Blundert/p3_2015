package com.matteogranzotto.puzzlesolver;

public class Puzzle
{

    /*Variabili*/
    private PieceOfPuzzle [][] puzzle;
    private int r;
    private int c;

    /*Costruttori*/
    public Puzzle()
    {
        puzzle=null;
    }

    public Puzzle(int righe, int colonne) throws ExceptionPS /*righe, colonne*/
    {
        r=righe;
        c=colonne;
        puzzle=new PieceOfPuzzle[righe][colonne];
        try {
            for (int i = 0; i < righe; i++)
            {
                for (int j = 0; j < colonne; j++)
                {
                    puzzle[i][j] = null;
                }
            }
        }
        catch(ArrayIndexOutOfBoundsException e)
        {
            throw new ExceptionPS("Superato limite massimo dell'array." );
        }
    }

    public Puzzle(Puzzle puzzleDiCopia) /*Costruttore di copia*/
    {
        this.puzzle=puzzleDiCopia.puzzle;
    }

    /*Metodi*/
    public int numColonne()
    {
        return c;
    }

    public int numRighe()
    {
        return r;
    }

    public PieceOfPuzzle getElement(int riga, int colonna) throws ExceptionPS
    {
        try
        {
            return puzzle[riga][colonna];
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            throw new ExceptionPS("Non è possibile recuperare l'elemento.");
        }
    }

    public void insertElement(PieceOfPuzzle tessera, int riga, int colonna) throws ExceptionPS
    {
        try
        {
            puzzle[riga][colonna] = tessera;
        }
        catch(ArrayIndexOutOfBoundsException e)
        {
            throw new ExceptionPS("["+riga+"]"+"["+colonna+"]"+"Non è possibile inserire l'elemento in questo posto.");
        }
    }

} //Fine classe