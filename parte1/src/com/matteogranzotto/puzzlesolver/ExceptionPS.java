package com.matteogranzotto.puzzlesolver;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ExceptionPS extends Exception
{

    /*Variabili*/
    private static Charset charset = StandardCharsets.UTF_8;
    private String err="";

    /*Costruttori*/
    public ExceptionPS(String e)
    {
        err=e;
    }

    /*Metodi*/
    public String getError()
    {
        return err;
    }

    public void writeError(String output)
    {
        writeContent(Paths.get(output),err);
    }

    /*Metodi statici*/
    private static void writeContent(Path file, String ex)
    {
        try (BufferedWriter writer = Files.newBufferedWriter(file, charset))
        {
                writer.write(ex);
        }
        catch (IOException e)
        {
            System.err.println(e);
        }
    }

} //Fine classe
