package com.matteogranzotto.puzzlesolver;

import java.rmi.*;
import java.util.ArrayList;
import java.io.Serializable;

public interface PuzzleResolver extends Remote {

    /*Metodi*/
    //Ritorna un Puzzle corretto
   // public Puzzle getPuzzleSolved () throws RemoteException;

    public Puzzle solvePuzzle(ArrayList<PieceOfPuzzle> array) throws ExceptionPS, InterruptedException, RemoteException;

} // Fine interfaccia
