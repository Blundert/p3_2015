package com.matteogranzotto.puzzlesolver;

import java.util.ArrayList;
import java.io.Serializable;

public class FindRow extends Thread
{

    /*Variabili*/
    PieceOfPuzzle pop;
    ArrayList<PieceOfPuzzle> array;
    ArrayList<PieceOfPuzzle> arrayp;
    int indie;
    Puzzle puzzle;

    /*Costruttori*/
    public FindRow(Puzzle pu, PieceOfPuzzle p, ArrayList<PieceOfPuzzle> array, int i) {
        pop = p;
        this.array = array;
        indie = i;
        puzzle = pu;
    }

    /*Metodi*/
    public void run()
    {
        try
        {
           arrayp=getRigaAPartireDaElemento(pop, array);
           inserisci();
        }
        catch (ExceptionPS exceptionPS)
        {
            System.err.println(exceptionPS.getError());
        }
    }

    //Compone una riga a partire da un PieceOfPuzzle
    private ArrayList<PieceOfPuzzle> getRigaAPartireDaElemento(PieceOfPuzzle elemento, ArrayList<PieceOfPuzzle> array)
    {
        ArrayList<PieceOfPuzzle> temp = new ArrayList<PieceOfPuzzle>();
        PieceOfPuzzle ultimo = elemento;

        try
        {
            while (!ultimo.getEst().equalsIgnoreCase("VUOTO" ))
            {
                boolean stop = false;
                try
                {
                    for (int i = 0; i < array.size() && !stop; i++)
                    {
                        if (array.get(i).getOvest().equalsIgnoreCase(ultimo.getIdPezzo()))
                        {
                            temp.add(array.get(i));
                            ultimo = array.get(i);
                            stop = true;
                        }
                    }
                }
                catch (ArrayIndexOutOfBoundsException e) { }
            }
        }
        catch (ArrayIndexOutOfBoundsException e) { }
        return temp;
    }

    public void inserisci() throws ExceptionPS
    {
        setRigaDopoElemento(arrayp,indie);
    }

    //Inserisce in riga un numero di elementi a partire da un elemento dato
    private void setRigaDopoElemento(ArrayList<PieceOfPuzzle> arrayp,int elementoDellaDiagonale) throws ExceptionPS
    {
        synchronized (puzzle)
        {
            try
            {
                for (int i = elementoDellaDiagonale; i < arrayp.size() + elementoDellaDiagonale; i++)
                {
                    puzzle.insertElement(arrayp.get(i - elementoDellaDiagonale), elementoDellaDiagonale, i + 1);
                }
            }
            catch (ArrayIndexOutOfBoundsException e)
            {
                throw new ExceptionPS("Sei uscito dal limite massimo dell'array.");
            }
        }
    }

} //Fine class
