package com.matteogranzotto.puzzlesolver;

import java.rmi.Naming;
import java.io.Serializable;
import java.rmi.RemoteException;

public class PuzzleSolverServer
{

    /*Metodi*/
    public static void main(String[] args)  throws Exception, ExceptionPS
    {
        /*Variabili*/
        String HOST = "";

        try
        {
            if (args.length > 0)
            {
                HOST = args[0];
            }
            PuzzleResolverImplementation ref = new PuzzleResolverImplementation();
            String rmiObjName = "rmi://" + HOST + "/PuzzleResolver";
            Naming.rebind(rmiObjName, ref);
            System.out.println("Server pronto.");
        }
        catch(RemoteException exc){
            System.out.println("Problemi di connessione, controllare che il registro RMI sia avviato e pronto all'uso.");
        }
        catch(Exception exc){
            System.out.println("Errore sconosciuto di rete.");
        }
    }


} //Fine classe
