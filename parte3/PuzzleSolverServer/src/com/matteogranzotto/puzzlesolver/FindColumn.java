package com.matteogranzotto.puzzlesolver;

import java.util.ArrayList;
import java.io.Serializable;

public class FindColumn extends Thread
{

    /*Variabili*/
    PieceOfPuzzle pop;
    ArrayList<PieceOfPuzzle> array;
    ArrayList<PieceOfPuzzle> arrayp;
    int indie;
    Puzzle puzzle;

    /*Costruttori*/
    public FindColumn( Puzzle pu, PieceOfPuzzle p, ArrayList<PieceOfPuzzle> array, int i)
    {
        pop=p;
        this.array=array;
        indie=i;
        puzzle=pu;
    }

    /*Metodi*/
    public void run()
    {
        try
        {
            arrayp=getColonnaAPartireDaElemento(pop,array);
            inserisci();
        }
        catch (ExceptionPS exceptionPS)
        {
            System.err.println(exceptionPS.getError());
        }
    }

    //Compone una colonna a partire da un PieceOfPuzzle
    private ArrayList<PieceOfPuzzle> getColonnaAPartireDaElemento(PieceOfPuzzle elemento, ArrayList<PieceOfPuzzle> array) throws ExceptionPS
    {
        ArrayList<PieceOfPuzzle> temp = new ArrayList<PieceOfPuzzle>();
        PieceOfPuzzle ultimo = elemento;

        try
        {
            while (!ultimo.getSud().equalsIgnoreCase("VUOTO" ))
            {
                boolean stop = false;
                try
                {
                    for (int i = 0; i < array.size() && !stop; i++)
                    {
                        if (array.get(i).getNord().equalsIgnoreCase(ultimo.getIdPezzo()))
                        {
                            temp.add(array.get(i));
                            ultimo = array.get(i);
                            stop = true;
                        }
                    }
                }
                catch (ArrayIndexOutOfBoundsException e)
                {
                    throw new ExceptionPS("Sei uscito dal limite massimo dell'array.");
                }
            }
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            throw new ExceptionPS("Sei uscito dal limite massimo dell'array.");
        }
        return temp;
    }

    public void inserisci() throws ExceptionPS
    {
        setColonnaDopoElemento(arrayp, indie);
    }

    //Inserisce in colonna un numero di elementi a partire da un elemento dato
    private void setColonnaDopoElemento(ArrayList<PieceOfPuzzle> arrayp,int elementoDellaDiagonale) throws ExceptionPS
    {
        synchronized (puzzle)
        {
            try
            {
                for (int i = elementoDellaDiagonale; i < arrayp.size() + elementoDellaDiagonale; i++)
                {
                    puzzle.insertElement(arrayp.get(i - elementoDellaDiagonale), i + 1, elementoDellaDiagonale);
                }
            }
            catch (ArrayIndexOutOfBoundsException e)
            {
                throw new ExceptionPS("Sei uscito dal limite massimo dell'array.");
            }
        }
    }

} //Fine classe
