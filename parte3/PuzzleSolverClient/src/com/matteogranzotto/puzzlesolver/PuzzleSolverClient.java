package com.matteogranzotto.puzzlesolver;

import java.rmi.*;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.NotBoundException;
import java.rmi.ConnectException;

public class PuzzleSolverClient
{

    /*Metodi*/
    public static void main(String[] args)
    {
        try
        {
            /*Variabili*/
            //Ottengo i percorsi dei file
            String inputFile = "";
            String outputFile = "";
            String HOST = "";

            try
            {
                if (args.length > 2)
                {
                    inputFile = "../../"+args[0];
                    outputFile = "../../"+args[1];
                    HOST = args[2];
                }
                else
                {
                    throw new ExceptionPS("Non sono stati inseriti i file di input e di output.");
                }
                ReadPiecesFromInputFile input = new ReadPiecesFromInputFile(inputFile);
                PuzzleResolver resolvePuzzle = (PuzzleResolver) Naming.lookup("rmi://"+HOST+"/PuzzleResolver");
                Puzzle puzzleSolved = resolvePuzzle.solvePuzzle(input.getArrayOfPieces());
                WritePuzzleInOutputFile output = new WritePuzzleInOutputFile(outputFile, puzzleSolved);
            }
            catch (ExceptionPS e)
            {
                if (outputFile == "") //stampo errrore a video
                {
                    System.err.println(e.getError());
                }
                else //se ho un file di output lo scrivo in quest'ultimo
                {
                    e.writeError(outputFile);
                }
            }
            catch (InterruptedException e)
            {
                System.out.println("Errore di rete");
             }
        }
        catch(RemoteException exc){
            System.out.println("Problemi di connessione al server, controllare la connessione e riprovare.");
        }
        catch(NotBoundException exc){
            System.out.println("Nome non presente nel server RMI.");
        }
        catch(Exception exc){
            System.out.println("Errore sconosciuto di rete.");
        }
    }

} // Fine classe
