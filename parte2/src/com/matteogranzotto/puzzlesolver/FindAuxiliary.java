package com.matteogranzotto.puzzlesolver;

import java.util.ArrayList;

public class FindAuxiliary extends Thread
{

    /*Varaibili*/
    private String whatt="";
    private int c,r;
    ArrayList<PieceOfPuzzle> array;

    /*Costruttori*/
    public FindAuxiliary(String cc, ArrayList<PieceOfPuzzle> array) throws ExceptionPS
    {
        if(cc=="colonna" || cc=="riga")
        {
            whatt=cc;
            this.array=array;
        }
        else
        {
            throw new ExceptionPS("Non hai dato il giusto comando.");
        }
    }

    /*Metori*/
    public void run()
    {
        if(whatt=="colonna")
        {
            try
            {
                c=trovaNumeroColonne(array);
            }
            catch (ExceptionPS e)
            {
                System.err.println(e.getError());
            }
        }
        else
        {
            if(whatt=="riga")
            {
                try
                {
                    r=trovaNumeroRighe(array);
                }
                catch (ExceptionPS e)
                {
                        System.err.println(e.getError());
                }
            }
        }
    }

    public int getC()
    {
        return c;
    }

    public int getR()
    {
        return r;
    }

    //Trova il numero di righe che comporranno il Puzzle
    private int trovaNumeroRighe(ArrayList<PieceOfPuzzle> array) throws ExceptionPS
    {
        int cont=0;

        try {
            for (int i = 0; i < array.size(); i++)
            {
                if (array.get(i).getOvest().equalsIgnoreCase("VUOTO" ))
                {
                    cont++;
                }
            }
        }
        catch(ArrayIndexOutOfBoundsException e)
        {
            throw new ExceptionPS("Ci sono stati dei problemi nella lettura del file.");
        }
        return cont;
    }

    //Trova il numero di colonne che comporranno il Puzzle
    private int trovaNumeroColonne(ArrayList<PieceOfPuzzle> array) throws ExceptionPS
    {
        int cont=0;

        try {
            for (int i = 0; i < array.size(); i++)
            {
                if (array.get(i).getNord().equalsIgnoreCase("VUOTO" ))
                {
                    cont++;
                }
            }
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            throw new ExceptionPS("Ci sono stati dei problemi nella lettura del file.");
        }
        return cont;
    }

} // Fine classe
