package com.matteogranzotto.puzzlesolver;

public class Puzzle
{

    /*Variabili*/
    private PieceOfPuzzle [][] puzzle;
    private int r=0;
    private int c=0;

    /*Costruttori*/
    public Puzzle()
    {
        puzzle=null;
    }

    public Puzzle(int righe, int colonne) throws ExceptionPS/*righe, colonne*/
    {
        r=righe;
        c=colonne;
        puzzle=new PieceOfPuzzle[righe][colonne];

        try
        {
            for (int i = 0; i < righe; i++)
            {
                for (int j = 0; j < colonne; j++)
                {
                    puzzle[i][j] = null;
                }
            }
        }
        catch(ArrayIndexOutOfBoundsException e)
        {
            throw new ExceptionPS("Sei uscito dal limite dell'array.");
        }
    }

    public Puzzle(Puzzle puzzleDiCopia) /*Costruttore di copia*/
    {
        this.puzzle=puzzleDiCopia.puzzle;
    }

    /*Metodi*/
    public int numColonne()
    {
        return c;
    }

    public int numRighe()
    {
        return r;
    }

    public PieceOfPuzzle getElement(int riga, int colonna)
    {
            return puzzle[riga][colonna];
    }

    public void insertElement(PieceOfPuzzle tessera, int riga, int colonna) throws ExceptionPS
    {
        try
        {
            puzzle[riga][colonna] = tessera;
        }
        catch(ArrayIndexOutOfBoundsException e)
        {
            throw new ExceptionPS("Non puoi inserire l'elemento qui.");
        }
    }

} // Fine classe