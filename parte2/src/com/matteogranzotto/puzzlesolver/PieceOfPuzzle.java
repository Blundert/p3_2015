package com.matteogranzotto.puzzlesolver;

public class PieceOfPuzzle
{

    /*Variabili*/
    private String id_pezzo=null;
    private String carattere=null;
    private String id_nord="VUOTO";
    private String id_est="VUOTO";
    private String id_sud="VUOTO";
    private String id_ovest="VUOTO";

    /*Costruttori*/
    public PieceOfPuzzle() {}

    public PieceOfPuzzle(String id_pezzo, String carattere)
    {
        this.id_pezzo=id_pezzo;
        this.carattere=carattere;
    }

    public PieceOfPuzzle(String id_pezzo, String carattere, String id_nord)
    {
        this.id_pezzo=id_pezzo;
        this.carattere=carattere;
        this.id_nord=id_nord;
    }

    public PieceOfPuzzle(String id_pezzo, String carattere, String id_nord, String id_est)
    {
        this.id_pezzo=id_pezzo;
        this.carattere=carattere;
        this.id_nord=id_nord;
        this.id_est=id_est;
    }

    public PieceOfPuzzle(String id_pezzo, String carattere, String id_nord, String id_est, String id_sud)
    {
        this.id_pezzo=id_pezzo;
        this.carattere=carattere;
        this.id_nord=id_nord;
        this.id_est=id_est;
        this.id_sud=id_sud;
    }

    public PieceOfPuzzle(String id_pezzo, String carattere, String id_nord, String id_est, String id_sud, String id_ovest)
    {
        this.id_pezzo=id_pezzo;
        this.carattere=carattere;
        this.id_nord=id_nord;
        this.id_est=id_est;
        this.id_sud=id_sud;
        this.id_ovest=id_ovest;
    }

    /*Metodi*/
    public void setIdPezzo(String id_pezzo)
    {
        this.id_pezzo=id_pezzo;
    }

    public void setCarattere(String carattere)
    {
        this.carattere=carattere;
    }

    public void setNord(String id_nord)
    {
        this.id_nord=id_nord;
    }

    public void setEst(String id_est)
    {
        this.id_est=id_est;
    }

    public void setSud(String id_sud)
    {
        this.id_sud=id_sud;
    }

    public void setOvest(String id_ovest)
    {
        this.id_ovest=id_ovest;
    }

    public String getIdPezzo()
    {
        return id_pezzo;
    }

    public String getCarattere()
    {
        return carattere;
    }


    public String getNord()
    {
        return id_nord;
    }

    public String getEst()
    {
        return id_est;
    }

    public String getSud()
    {
        return id_sud;
    }

    public String getOvest()
    {
        return id_ovest;
    }

    public String toString()
    {
        return "'"+carattere+"'["+id_pezzo+"](["+id_nord+"]"+"["+id_est+"]"+"["+id_sud+"]"+"["+id_ovest+"])";
    }

} // Fine classe
