package com.matteogranzotto.puzzlesolver;

import java.util.ArrayList;

public class PuzzleResolver
{

    /*Variabili*/
    private ArrayList<PieceOfPuzzle> array;
    private ArrayList<PieceOfPuzzle> diagonale;
    private Puzzle puzzle;

    /*Costruttori*/
    //Costruttore di default.
    public PuzzleResolver()
    {
        array=null;
        puzzle=null;
    }

    //Costruttore ad un parametro
    public PuzzleResolver(ArrayList<PieceOfPuzzle> array) throws InterruptedException, ExceptionPS
    {
        this.array=array;
        Solver();
    }

    /*Metodi */
    //Ritorna un Puzzle corretto
    public Puzzle getPuzzleSolved ()
    {
        return puzzle;
    }

    //Posso sapere se l'array di PieceOfPuzzle � stato risolto e posso ottenerlo con la chiamata getPuzzleSolved
    public boolean isResolve()
    {
        return array==null;
    }

    //Posso sapere se PuzzleResolver ha ricevuto in input un array di PieceOfPuzzle
    public boolean isInizializate()
    {
        return puzzle==null && array==null;
    }

    private void Solver() throws InterruptedException, ExceptionPS
    {
        /*Trovo il numero di righe e colonne con due thred che si aspettano con il join e poi creo il nuovo puzzle*/
        FindAuxiliary f1=new FindAuxiliary("colonna",array);
        FindAuxiliary f2=new FindAuxiliary("riga",array);
        f1.start();
        f2.start();
        try
        {
            f1.join();
            f2.join();
        }
        catch(InterruptedException e) { }
        int c = f1.getC();
        int r = f2.getR();

        puzzle=new Puzzle(r,c);
        /*trovo la diagonale */
        diagonale=trovaDiagonale(array,r,c);
        try
        {
            for (int i = 0; i < diagonale.size(); i++)
            {
                puzzle.insertElement(diagonale.get(i), i, i);
            }
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            throw new ExceptionPS("Sei uscito dal limite massimo dell'array.");
        }
        /*dalla diagonale ora lancio due thread, uno per la riga e uno per la colonna, per ogni elemento per trovare  */
        ArrayList<FindRow> row= new ArrayList<FindRow>();
        ArrayList<FindColumn> col= new ArrayList<FindColumn>();
        try
        {
            for (int i = 0; i < diagonale.size(); i++)
            {
                row.add(new FindRow(puzzle, diagonale.get(i), array, i));
                col.add(new FindColumn(puzzle, diagonale.get(i), array, i));
                row.get(i).start();
                col.get(i).start();
            }
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            throw new ExceptionPS("Sei uscito dal limite massimo dell'array.");
        }
        try
        {
            for (int i = 0; i <row.size();i++)
            {
                row.get(i).join();
            }
            for(int i=0;i<col.size();i++)
            {
                col.get(i).join();
            }
        }
        catch(InterruptedException e) { }
        catch (ArrayIndexOutOfBoundsException e)
        {
            throw new ExceptionPS("Sei uscito dal limite massimo dell'array.");
        }
    }

    public ArrayList<PieceOfPuzzle> trovaDiagonale(ArrayList<PieceOfPuzzle> a, int r, int c) throws InterruptedException, ExceptionPS
    {
        ArrayList<PieceOfPuzzle> temp= new ArrayList<PieceOfPuzzle>();
        String nord="VUOTO";
        String ovest="VUOTO";
        int ld=r; //Lunghezza diagonale, so a priori che è lunga quanto il lato più piccolo
        if(c<r)
            ld=c;
        PieceOfPuzzle elemento =null;

        try
        {
            for (int i = 0; i < ld; i++)
            {
                //trovo il primo elemento della diagonale
                elemento = trovaPrimoElementoDiagonale(nord, ovest);
                temp.add(elemento);
                if (i < ld - 1)
                {
                    nord = elemento.getEst();
                    ovest = elemento.getSud();
                }
            }
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            throw new ExceptionPS("Sei uscito dal limite massimo dell'array.");
        }
        return temp;
    }

    //Trova il primo elemento della diagonale
    private PieceOfPuzzle trovaPrimoElementoDiagonale(String nord, String ovest)  //trova il primo elemento della diagonale e lo toglie dall'array
    {
        PieceOfPuzzle temp=null;
        boolean trovato=false;
        try
        {
            for (int i = 0; i < array.size() && !trovato; i++)
            {
                if (array.get(i).getNord().equalsIgnoreCase(nord) && array.get(i).getOvest().equalsIgnoreCase(ovest))
                {
                    temp = array.get(i);
                    array.remove(i);
                    trovato=true;
                }
            }
        }
        catch (ArrayIndexOutOfBoundsException e) { }
        return temp;
    }

} // Fine classe

/**
 * L'oggetto puo essere usato in due modi:
 * I)  Creo un oggetto di tipo PuzzleResolver passandogli come unico parametro un array di PieceOfPuzzle.
 *     Questo, se l'array e corretto e non vuoto, crea Puzzle risolto che si puo ottenere con la chiamata
 *     del metodo getPuzzleSolved.
 * II) Creo un oggetto vuoto di tipo PuzzleResolver. In un secondo momento chiamamo il metodo getPuzzleSolved,
 *     a cui passo un array corretto di PieceOfPuzzle, il quale restituisce un Puzzle corretto.
 */
