package com.matteogranzotto.puzzlesolver;

import java.io.File;
import java.lang.Exception;
import java.nio.file.FileSystemException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main
{

    /*Metodi*/
    public static void main(String[] args) throws ExceptionPS
    {

        /*Variabili*/
        //Ottengo i percorsi dei file
        String inputFile ="";
        String outputFile ="";

        try
        {
            if (args.length>=2)
            {
                inputFile = "../"+args[0];
                outputFile = "../"+args[1];
            }
            else
            {
                throw new ExceptionPS("Non sono stati inseriti i file di input e di output." );
            }
            ReadPiecesFromInputFile input = new ReadPiecesFromInputFile(inputFile);
            PuzzleResolver resolvePuzzle = new PuzzleResolver(input.getArrayOfPieces());
            Puzzle puzzleSolved = resolvePuzzle.getPuzzleSolved();
            WritePuzzleInOutputFile output = new WritePuzzleInOutputFile(outputFile, puzzleSolved);
        }
        catch (InterruptedException e)
        {
            throw new ExceptionPS("C'è stato un errore.");
        }
        catch(ExceptionPS e)
        {
            if(outputFile=="") //stampo errrore a video
            {
                System.err.println(e.getError());
            }
            else //se ho un file di output lo scrivo in quest'ultimo
            {
                e.writeError(outputFile);
            }
        }

    }

} // Fine classe
